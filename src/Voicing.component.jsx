import React, { Component } from 'react'

import JSZip from 'jszip'
import saveAs from 'file-saver';

import "milligram"

import TextContainer from "./text/textContainer.component"
import Recorder from "./recorder/recorder.component"
import List from "./list/list.component"

class Voicing extends Component {

    constructor(props) {
        super(props)

        this.state = {
            textArr: null,
            textIndex: 0,
            recorded: 0,
            recordedTime: 0,
            recordingsSize: 0,
            showList: false
        }
    }

    updateAppState = (obj, callback) => {
        this.setState( obj, () => {
            if (callback) callback()
        })
    }


    //shared between recorder and text
    nextText = () => {

        let i = this.state.textIndex
        if (i < this.state.textArr.length - 1) {
            i++
            this.setState({textIndex: i})
            this.clearing()
        }
    }

    clearing = () => {

        let i = this.state.textIndex
        let textArr = this.state.textArr
        let text = textArr[i]

        if (text.stream) {
            text.stream.stop()
            text.stream = null
        }

        if (text.recordRTC) {
            text.recordRTC.stopRecording()
        }
        text.isRecording = false
        clearInterval(text.updateRecTimerIntervalId)

        if (text.html5Audio) {
            text.html5Audio.pause()
            text.html5Audio.currentTime = 0
        }
        text.isPlaying = false
        clearInterval(text.updatePlayTimerIntervalId)
        text.playingTime = null

        this.setState({textArr})
    }

    //--


    downloadRecords = () => {
        let isSomethingRecorded = false
        let textArr = this.state.textArr

        textArr.forEach(text => {
            if (text.record) isSomethingRecorded = true
        })

        if (isSomethingRecorded) {
            let zip = new JSZip()
            let recorded = []
            let notRecorded = []
            let withComments = []

            for (let i=0; i<this.state.textArr.length; i++) {
                let text = this.state.textArr[i]

                if (text.record) {
                    let record = text.record.replace("data:audio/wav;base64,", "")
                    zip.file(text.id+'.wav', record, {base64:true})
                    recorded.push({id: text.id, text:text.text})

                    text.isDownloaded = true
                } else {
                    notRecorded.push({id: text.id, text:text.text})
                }

                if (text.comment) {
                    withComments.push({id: text.id, text:text.text, comment: text.comment})
                }
            }

            zip.file('_recorded.json', JSON.stringify(recorded))

            // if (notRecorded.length > 0 ) {
            //     zip.file('_not-recorded.json', JSON.stringify(notRecorded))
            // }

            if (withComments.length > 0 ) {
                zip.file('_comments.json', JSON.stringify(withComments))
            }

            let d = new Date()
            d = d.getTime()

            zip.generateAsync({type:"blob"})
                .then( (content) => {
                    saveAs(content, "records_" + d + ".zip")

                });

            textArr.forEach(text => {
                if (text.record) text.record = null
            })


            this.setState({
                recordedTime: 0,
                recordingsSize: 0,
                textArr
            })
        }
    }

    downloadComments = () => {
        let isSomethingCommented = false
        let textArr = this.state.textArr

        textArr.forEach(text => {
            if (text.comment) isSomethingCommented = true
        })

        if (isSomethingCommented) {
            let textInfo = []

            for (let i=0; i<this.state.textArr.length; i++) {
                let text = this.state.textArr[i]

                if (text.comment) {
                    textInfo.push({id: text.id, text:text.text, comment: text.comment})
                }
            }

            textInfo = JSON.stringify(textInfo)
            saveAs(new Blob([textInfo], {type: "text/plain;charset=utf-8"}), 'comments.json')

        }
    }

    loadTextBtnClick = () => {
        this.refs.fileUploader.click()
    }

    onFile = (e) => {
        let file = e.target.files[0]

        if (file) {
            let reader = new FileReader();
            reader.readAsText(file)

            reader.onload = (e => {
                let json = JSON.parse(reader.result)
                this.setState({ textArr: json } )
            })
        }
    }

    render() {

        let text
        let textArr = this.state.textArr

        let skippedCounter = 0

        if (textArr) {
            let i = this.state.textIndex
            text = textArr[i]
            textArr.forEach( (t, j) => {
                if ( !t.isRecorded && j < i ) {
                    skippedCounter++
                }
            })
        }

        let recorded = this.state.recorded

        if (text) {

            return (
                <div style={{padding:"10px"}} align="center">

                    {
                        this.state.showList ? (
                            <List
                                updateAppState={this.updateAppState}
                                appState={this.state} />
                        ) : null
                    }

                    <div style={{display: this.state.showList ? 'none' : 'block'}}>
                        <TextContainer
                            _recKeyListener = {this.state._recKeyListener}
                            nextText={this.nextText}
                            clearing={this.clearing}
                            appState={this.state}
                            updateAppState={this.updateAppState}
                            text={text}/>

                        <Recorder   downloadRecords={this.downloadRecords}
                                    appState={this.state}
                                    nextText={this.nextText}
                                    updateAppState={this.updateAppState}/>

                        <div align="center">
                            Not Recorded:
                            <span style={{fontSize: "30px", color:"red"}}> { textArr.length - recorded } </span>,
                            Skipped:
                            <span style={{fontSize: "30px", color:"red"}}> { skippedCounter } </span>,

                            {/*<span onClick= { () => {*/}
                            {/*this.setState((state) =>*/}
                            {/*state.showList = !this.state.showList*/}
                            {/*)*/}
                            {/*}}*/}
                            {/*style={{cursor:"pointer", margin:"5px"}}>*/}
                            {/*{ this.state.showList ? "Close list" : "show list" }*/}
                            {/*</span>*/}

                        </div>


                        <button onClick={this.downloadRecords} style={{margin:"5px"}}>
                            Download records
                        </button>

                        <button onClick={this.downloadComments} style={{margin:"5px"}}>
                            Download comments
                        </button>

                        <br />
                        Keys: <br />
                        Space - start/stop recording and playing <br />
                        Arrows - prev/next <br />
                        Esc - delete record for current text <br />
                        Home/End - first/last
                    </div>

                </div>
            )
        } else {
            return (
                <div align="center" style={{paddingTop:"20px"}}>

                    <input
                        style={{ display: 'none' }}
                        id="raised-button-file"
                        type="file"
                    />
                    <label htmlFor="raised-button-file">
                        <button onClick={this.loadTextBtnClick}>
                            load text
                            <input onChange={this.onFile} type="file" id="file" ref="fileUploader" style={{display: "none"}}/>
                        </button>
                    </label>

                    <br />
                    Input file format JSON: [ {'{ "id": any, "text": string }'} ]
                    <br />
                    <br />
                    "id" will be name of the record file: id+'.wav'
                </div>
            )
        }
    }


}

export default Voicing;
