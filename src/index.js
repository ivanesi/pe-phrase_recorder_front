import React from 'react'
import ReactDOM from 'react-dom'
import Voicing from './Voicing.component'
import * as serviceWorker from './serviceWorker'

ReactDOM.render(<Voicing />, document.getElementById('root'))


serviceWorker.unregister()
