import React, { Component } from 'react'

import ListItem from './listItem.component'

class List extends Component {

    constructor(props) {
        super(props)

        this.state = {
            list: this.props.appState.textArr
        }
    }

    showSkipped = () => {
        let skipped = []
        let textArr = JSON.parse(JSON.stringify(this.props.appState.textArr))

        let i = this.props.appState.textIndex

        textArr.forEach( (t, j) => {
            if ( !t.isRecorded && j < i ) {
                t.num = j + 1
                skipped.push(t)
            }
        })

        this.setState({list: skipped})

    }

    showNotRecorded = () => {
        let notRecorded = []
        let textArr = JSON.parse(JSON.stringify(this.props.appState.textArr))

        textArr.forEach( (t, j) => {
            if ( !t.isRecorded  ) {
                t.num = j + 1
                notRecorded.push(t)
            }
        })

        this.setState({list: notRecorded})
    }

    showNotDownloaded = () => {
        let notDownloaded = []
        let textArr = JSON.parse(JSON.stringify(this.props.appState.textArr))

        textArr.forEach( (t, j) => {
            if ( t.isRecorded && !t.isDownloaded ) {
                t.num = j + 1
                notDownloaded.push(t)
            }
        })

        this.setState({list: notDownloaded})
    }

    showAll = () => {
        let list = JSON.parse(JSON.stringify(this.props.appState.textArr))
        this.setState({list})
    }


    onListItemClick = (num, index) => {
        console.log(num, index)
        this.props.updateAppState({
            textIndex:index
        })

        this.openCloseList()
    }

    openCloseList = () => {
        this.props.updateAppState({
            showList: !this.props.appState.showList,
        })
    }

    render () {

        let list = this.state.list

        list = list.map( (item, index)  => {

            return <ListItem onListItemClick = {this.onListItemClick}
                             key={item.id}
                             num={item.num}
                             index = {index}
                             text={item.text}
                             isRecorded={item.isRecorded}
                             isDownloaded={item.isDownloaded} />
        })


        return (
            <div>
                <div>
                    <button onClick={this.showSkipped} style={{margin:"5px"}}>
                        Skipped
                    </button>
                    <button onClick={this.showNotRecorded} style={{margin:"5px"}}>
                        Not Recorded
                    </button>
                    <button onClick={this.showNotDownloaded} style={{margin:"5px"}}>
                        Recorded but not Downloaded
                    </button>
                    <button onClick={this.showAll} style={{margin:"5px"}}>
                        All
                    </button>
                    <button onClick={this.openCloseList}>
                        Close
                    </button>
                </div>


                <table>
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Text</th>
                        <th>Recorded</th>
                        <th>Downloaded</th>
                    </tr>
                    </thead>
                    <tbody>
                        {list}
                    </tbody>
                </table>

            </div>
        )
    }
}

export default List