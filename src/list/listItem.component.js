import React, { Component } from 'react'

class ListItem extends Component {

    render() {

        return (
                <tr style={{cursor:"pointer"}} onClick= { this.props.onListItemClick.bind(this, this.props.num, this.props.index) }>
                    <td>{this.props.num ? this.props.num : this.props.index + 1 }</td>
                    <td>{this.props.text.substr(0,120) +'...'}</td>
                    <td>{this.props.isRecorded ? 'yes' :' no'}</td>
                    <td>{this.props.isDownloaded ? 'yes' :' no'}</td>
                </tr>

        )
    }

}

export default ListItem