import React, { Component } from 'react'

class RecButton extends Component {

    render() {
        if (this.props.show) {
            return (
                <button onClick={this.props.onClick}
                     style={{margin: "10px"}}
                     color={this.props.color}
                     >

                    {this.props.text}

                </button>
            )
        } else return null
    }
}

export default RecButton