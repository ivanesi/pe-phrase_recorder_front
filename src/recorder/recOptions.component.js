import React, { Component } from 'react'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormLabel from '@material-ui/core/FormLabel'

class RecOptions extends Component {

    constructor(props) {
        super(props)

        this.state = {
            currentDevice: "default"
        }
    }

    changeInputDevice = (event) => {
        let deviceId = event.target.value
        this.setState({ currentDevice: deviceId });

        navigator.mediaDevices.getUserMedia({
            audio: { deviceId }
        });
    }

    render() {

        let devices = this.props.devices.map(d =>
            <FormControlLabel
                value={d.deviceId}
                key={d.deviceId}
                control={<Radio />}
                label={d.label}/>
        )

        return (
            <FormControl component="fieldset" style={{}}>

                <FormLabel component="legend"> Input Devices </FormLabel>

                <RadioGroup
                    aria-label="Input"
                    name="Input"
                    value={this.state.currentDevice}
                    onChange={this.changeInputDevice}
                >

                    {devices}
                </RadioGroup>

            </FormControl>
        )

    }
}

export default RecOptions