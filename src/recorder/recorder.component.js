import React, { Component } from 'react'

import RecordRTC from 'recordrtc';

import RecButton from "./recButton.component"

class Recorder extends Component {

    componentDidMount() {

        window.addEventListener('keydown', this._recKeyListener);

        this.props.updateAppState({_recKeyListener:this._recKeyListener})
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this._recKeyListener);
    }

    _recKeyListener = (e) => {

        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        if (e.code === 'Space' && !text.showPlayerBtns) {
            e.preventDefault()
            text.isRecording ? this.stopRecBtnOnClick() : this.startRecBtnOnClick()
        }

        if (e.code === 'Space' && text.showPlayerBtns) {
            e.preventDefault()
            text.isPlaying ? this.stopAudioBtnOnClick() : this.playAudioBtnOnClick()
        }

        if (e.code === 'Escape' && text.showPlayerBtns) {
            e.preventDefault()
            this.cancelRecBtnOnClick()
        }

    }

    createMediaStream = (callback) => {
        let mediaConstraints = { video:false, audio:true }

        navigator.mediaDevices.getUserMedia(mediaConstraints)
            .then(callback)
            .catch(err => console.log(err))

    }

    // rec

    startRecBtnOnClick = () => {

        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        this.createMediaStream(stream => {
            let StereoAudioRecorder = RecordRTC.StereoAudioRecorder;

            text.stream = stream
            text.isRecording = true
            text.recordRTC = new RecordRTC(stream, {
                recorderType: StereoAudioRecorder,
                mimeType: 'audio/wav'
            })

            this.props.updateAppState({textArr}, () => {
                this.props.appState.textArr[i].recordRTC.startRecording()
                this.startRecTime()
            })

        })
    }

    stopRecBtnOnClick = () => {

        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        let recorded = this.props.appState.recorded
        let recordedTime = this.props.appState.recordedTime
        let recordingsSize = this.props.appState.recordingsSize

        text.recordRTC.stopRecording(() => {
            text.recordRTC.getDataURL(dataUrl => {
                text.stream.stop()
                text.record = dataUrl
                text.html5Audio = new Audio(dataUrl)
                text.isRecording = false
                text.showPlayerBtns = true
                text.isRecorded = true

                recorded ++
                recordedTime += text.recTime
                recordingsSize += this._getSizeFromBase64(dataUrl)

                this.checkIfNeedToDownloadRecords(recordedTime)

                this.props.updateAppState({recorded, recordedTime, recordingsSize, textArr}, () => {
                    this.props.appState.textArr[i].recordRTC.stopRecording()
                    this.stopRecTime()
                    this.props.nextText()
                    this.checkIfNeedToDownloadRecords(recordedTime)
                })

            })
        })

    }

    // rec time

    startRecTime = () => {

        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        let startRecDate = Date.now()

        text.updateRecTimerIntervalId = setInterval(() => {
            text.recTime = Date.now() - startRecDate
            this.props.updateAppState({textArr})
        }, 100)

        this.props.updateAppState({textArr})
    }

    stopRecTime = () => {

        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        clearInterval(text.updateRecTimerIntervalId)

        this.props.updateAppState({textArr})

    }

    //--

    // play

    playAudioBtnOnClick = () => {
        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        text.html5Audio.play()
        text.isPlaying = true

        this.props.updateAppState({textArr}, () => {
            this.startPlayingTime(text.html5Audio)
        })
    }


    stopAudioBtnOnClick = () => {
        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        text.html5Audio.pause()
        text.html5Audio.currentTime = 0

        clearInterval(text.updatePlayTimerIntervalId)

        text.isPlaying = false
        text.playingTime = null

        this.props.updateAppState({textArr})

    }

    // play time

    startPlayingTime = (html5Audio) => {
        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        text.updatePlayTimerIntervalId = setInterval(() => {

            let durationInMs = parseInt(html5Audio.duration * 1000)
            let currentTimeInMs = parseInt(html5Audio.currentTime * 1000)

            if (text.playingTime !== 0) {
                text.playingTime = durationInMs - currentTimeInMs
                this.props.updateAppState({textArr})
            } else {
                clearInterval(text.updatePlayTimerIntervalId)
                text.playingTime = null
                text.isPlaying = false
                this.props.updateAppState({textArr})
            }
        }, 100)

        this.props.updateAppState({textArr})

    }

    // --

    cancelRecBtnOnClick = () => {
        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        let recordingsSize = this.props.appState.recordingsSize
        let recordSize = this._getSizeFromBase64(text.record)
        recordingsSize -= recordSize

        let recorded = this.props.appState.recorded
        let recordedTime = this.props.appState.recordedTime

        if (text.stream) {
            text.stream.stop()
            text.stream = null
        }

        text.recordRTC = null
        if (text.html5Audio) {
            text.html5Audio.pause()
            text.html5Audio = null
        }

        text.isPlaying = false
        text.playingTime = null
        clearInterval(text.updatePlayTimerIntervalId)

        recordedTime -= text.recTime

        text.showPlayerBtns = false

        if (text.record) {
            text.record = null
            recorded--

        }

        text.isRecorded = false

        this.props.updateAppState({recorded, recordedTime, recordingsSize, textArr})

    }

    downloadRecordBtnOnClick = () => {
        let i = this.props.appState.textIndex
        let textArr = this.props.appState.textArr
        let text = textArr[i]

        let filename = text.id+'.wav'

        let a = document.createElement('a');

        a.href = text.record;
        a.download = filename;

        a.click();

        text.record = null
        text.isDownloaded = true

        this.props.updateAppState({textArr})

    }

    checkIfNeedToDownloadRecords = (recordedTime) => {
        if (recordedTime / 1000  > 600 ) {
            this.props.downloadRecords()
        }
    }

    _millisecondsToTime(duration) {
        if (!duration) return null

        let milliseconds = parseInt((duration%1000)/100)
        let   seconds = parseInt((duration/1000)%60)
            , minutes = parseInt((duration/(1000*60))%60)
            , hours = parseInt((duration/(1000*60*60))%24)

        hours = (hours < 10) ? "0" + hours : hours
        minutes = (minutes < 10) ? "0" + minutes : minutes
        seconds = (seconds < 10) ? "0" + seconds : seconds

        return hours + ":" + minutes + ":" + seconds + ":" + milliseconds
    }

    _getSizeFromBase64(data) {
        data = data.split(',')[1]
        return window.atob(data).length
    }

    render() {

        let i = this.props.appState.textIndex
        let text = this.props.appState.textArr[i]

        return (

            <div align="center">

                <div align="center">
                    { text.isRecording ? <div style = {{ color:"white", width: "30%", background: "red", border:"5px solid red"}}> {this._millisecondsToTime(text.recTime)} </div> : null}
                    { text.isPlaying ? <div style = {{ color:"white", width: "30%", background: "blue", border:"5px solid blue"}}> {this._millisecondsToTime(text.playingTime)} </div> : null }
                </div>

                <RecButton
                    label = "startRec"
                    show = {!text.isRecording && !text.showPlayerBtns}
                    onClick = {this.startRecBtnOnClick}
                    text = "Rec"
                    color = "secondary"
                />
                <RecButton
                    text="Stop"
                    label = "StopRec"
                    show = {text.isRecording}
                    onClick = {this.stopRecBtnOnClick}
                    color = "primary"
                />

                <RecButton
                    label = "Play"
                    text = "Play"
                    show = { text.showPlayerBtns && !text.isPlaying }
                    onClick = {this.playAudioBtnOnClick}
                    color = "primary"
                />
                <RecButton
                    label = "StopPlay"
                    text="Stop"
                    show = { text.showPlayerBtns && text.isPlaying }
                    onClick = {this.stopAudioBtnOnClick}
                    color = "primary"
                />

                <RecButton
                    show = { text.record && !text.isRecording}
                    text="Del"
                    label = "cancel"
                    onClick = {this.cancelRecBtnOnClick}
                    color = "primary"
                />

                {
                    text.record
                        ? <button onClick={this.downloadRecordBtnOnClick}> Download </button>
                        : null
                }


            </div>

        );
    }

}

export default Recorder;
