import React, { Component } from 'react'

class Comment extends Component {
    constructor(props) {
        super(props)

        this.state = {
            comment: this.props.text.comment
        }
    }

    componentWillUnmount() {
        this.removeEventListeners()
    }

    handleChange = (event) => {
        let comment = event.target.value

        this.setState({
            comment
        })

        this.props.text.comment = comment
    }

    addEventListeners = () => {
        window.removeEventListener('keydown', this.props._recKeyListener)
        window.removeEventListener('keydown', this.props._textKeyListener)

    }

    removeEventListeners = () => {
        window.addEventListener('keydown', this.props._recKeyListener)
        window.addEventListener('keydown', this.props._textKeyListener)
    }


    render() {

        if (!this.props.text.comment) this.props.text.comment = ""

        return (
            <div style={{width:"80%"}}>
                <textarea
                    onBlur={this.removeEventListeners}
                    onFocus={this.addEventListeners}
                    style={{width:"100%", fontSize:"18px"}}
                    name="comment"
                    placeholder="comment it"
                    value={this.props.text.comment}
                    onChange={this.handleChange}
                />
            </div>
        );
    }
}

export default Comment;
