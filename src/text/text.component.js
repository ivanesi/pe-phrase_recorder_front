import React, { Component } from 'react'

class Text extends Component {

    render() {
        return (
            <div style = {{margin: "5px", verticalAlign:"middle", fontSize:"35px", width:"100%", height:"200px", border:"1px solid blue", overflow:"auto"}}>
                <p> {this.props.text} </p>
            </div>
        );
    }
}

export default Text;
