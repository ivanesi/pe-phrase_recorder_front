import React, { Component } from 'react'

import Text from './text.component'
import Comment from './comment.component'

class TextContainer extends Component {

    componentDidMount() {
        window.addEventListener('keydown', this._textKeyListener);
        this.props.updateAppState({_recKeyListener:this._textKeyListener})
    }

    _textKeyListener = (e) => {

        if (e.code === 'ArrowLeft') {
            e.preventDefault()
            this.prevText()
        }
        if (e.code === 'ArrowRight') {
            e.preventDefault()
            this.props.nextText()
        }
        if (e.code === 'Home') {
            e.preventDefault()
            this.firstText()
        }
        if (e.code === 'End') {
            e.preventDefault()
            this.lastText()
        }
    }


    firstText = () => {
        let i = 0
        this.props.updateAppState({textIndex: i}, () => {
            this.props.clearing()
        })


    }

    lastText = () => {
        let i = this.props.appState.textArr.length-1
        this.props.updateAppState({textIndex: i}, () => {
            this.props.clearing()
        })

    }

    prevText = () => {
        let i = this.props.appState.textIndex
        if (i > 0 ) {
            i--
            this.props.updateAppState({textIndex: i})
            this.props.clearing()
        }
    }

    render() {
        let text = this.props.text

        return (
            <div className={"container"}>
                <div align="center">

                    { text ? <Text text={text.text}/> : "" }

                </div>
                <div align="center">
                    <button style={{margin:"10px"}}
                        onClick={this.prevText}> Prev </button>

                    <button style={{margin:"10px"}}
                        onClick={this.props.nextText}> Next </button>
                </div>
                <div align="center">
                    <Comment
                        _textKeyListener = {this._textKeyListener}
                        _recKeyListener = {this.props._recKeyListener}
                        text={text}/>
                </div>
                <div align="center">
                    {this.props.appState.textIndex + 1 + " "}
                    /
                    {" " + this.props.appState.textArr.length}

                    <br />
                    <span>
                        Recorded: {text.isRecorded ? 'yes': 'no' },
                        Downloaded: {text.isDownloaded ? 'yes': 'no' }
                     </span>
                </div>
            </div>
        )
    }
}

export default TextContainer;
